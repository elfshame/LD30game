Duel of the witches

2 witches, 2 players
    duels through connected worlds

Fire Witch
P1 Fire button : R
P1 Charge button : T
P1 Aim : Hold R & T
WASD to move

Ice Witch
P2 Fire button : L
P1 Charge button : M
P1 Aim : Hold L & M
Arrow keys to move

Go to the portals. Once there you are invulnerable to your enemy and can charge power at will.

If five seconds next to each other without killing each other "Would you like to make peace?"
Y / N