document.onkeydown = function( e) {
    if (e.key == 'Up' || e.key == 'Down' || e.key == 'Left' || e.key == 'Right') {
            e.preventDefault();
    }
};

window.onload = function () {
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");

    // Create the canvas
    canvas.width = 1200;
    canvas.height = 720;
    ctx.imageSmoothingEnabled = false;
    ctx.webkitImageSmoothingEnabled = false;
    ctx.mozImageSmoothingEnabled = false;
    ctx.scale(3, 3);
    canvas.innerHTML = "Your browser doesn't support HTML5. Please update in order to play.";
    document.body.insertBefore(canvas, document.body.firstElementChild);

    // Background image
    var tilesetReady = false;
    var tilesetImage = new Image();
    tilesetImage.onload = function () {
        tilesetReady = true;
    };
    tilesetImage.src = "images/tileset.png";

    var bgReady = false;
    var bgImage = new Image();
    bgImage.onload = function () {
        bgReady = true;
    };
    bgImage.src = "images/background.png";

    var player1Ready = false;
    var player1Image = new Image();
    player1Image.onload = function () {
        player1Ready = true;
    };
    player1Image.src = "images/player1.png";

    var player2Ready = false;
    var player2Image = new Image();
    player2Image.onload = function () {
        player2Ready = true;
    };
    player2Image.src = "images/player2.png";

    var portalP1Ready = false;
    var portalP1Image = new Image();
    portalP1Image.onload = function () {
        portalP1Ready = true;
    };
    portalP1Image.src = "images/portalP1.png";

    var portalP2Ready = false;
    var portalP2Image = new Image();
    portalP2Image.onload = function () {
        portalP2Ready = true;
    };
    portalP2Image.src = "images/portalP2.png";

    // Game objects
    function Player() {
        this.speed = 128; // movement in pixels per second
        this.x = 0;
        this.y = 0;
        this.posx = 0;
        this.posy = 0;
    }
    var player1 = new Player();
    var player2 = new Player();
    function Portal(playerOwnerStr) {
        this.x = 0;
        this.y = 0;
        this.playerOwner = playerOwnerStr;
    }
    var portalP1 = new Portal("player1");
    var portalP2 = new Portal("player2");
    portalP2.x = canvas.width / 3 - 16;

    var scorePlayer1 = -1;
    var scorePlayer2 = 0;
    var level = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], []];

    var startScreen = true;
    setInterval(function(){
        startScreen = false;
    }, 3500);

    // Handle keyboard controls
    var keysDown = {};
    addEventListener("keydown", function (e) {
        keysDown[e.keyCode] = true;
    }, false);
    addEventListener("keyup", function (e) {
        delete keysDown[e.keyCode];
    }, false);

    var createNewLevel = function () {
        var i, j, c, d, randomness;
        for (i = 0, c = 15; i < c; i++) {
            for (j = 0, d = 25; j < d; j++) {
                if (j % 2 === 1) {
                    randomness = Math.random();
                    if (randomness > 0.45) {
                        level[i][j] = 1;
                    } else {
                    level[i][j] = 0;
                    }
                } else {
                    level[i][j] = 0;
                }
            }
        }
    };

// Reset the game when the player catches a portal
    var reset = function () {
        player1.x = canvas.width / 3 - 16;
        player1.y = canvas.height / (2 * 3);

        player2.x = 0;
        player2.y = canvas.height / (2 * 3);

        // Throw the portal somewhere on the screen randomly
        portalP1.y = Math.random() * (canvas.height / 3 - 32);
        portalP2.y = Math.random() * (canvas.height / 3 - 32);

        createNewLevel();
    };

    // Update game objects
    var update = function (modifier) {
        if (38 in keysDown && player1.y > 0 && level[player1.posy - 1][player1.posx] === 0) { // Player holding up
            player1.y -= player1.speed * modifier;
        }
        if (40 in keysDown && player1.y < canvas.height / 3 - 17 && level[player1.posy][player1.posx] === 0) { // Player holding down
            player1.y += player1.speed * modifier;
        }
        if (37 in keysDown && player1.x > 0 && level[player1.posy][player1.posx - 1] === 0) { // Player holding left
            player1.x -= player1.speed * modifier;
        }
        if (39 in keysDown && player1.x < canvas.width / 3 - 16 && level[(player1.posy)][player1.posx] === 0) { // Player holding right
            player1.x += player1.speed * modifier;
        }
        if (87 in keysDown && player2.y > 0 && level[player2.posy - 1][player2.posx] === 0) { // Player holding up
            player2.y -= player2.speed * modifier;
        }
        if (83 in keysDown && player2.y < canvas.height / 3 - 17 && level[player2.posy][player2.posx] === 0) { // Player holding down
            player2.y += player2.speed * modifier;
        }
        if (65 in keysDown && player2.x > 0 && level[player2.posy][player2.posx - 1] === 0) { // Player holding left
            player2.x -= player2.speed * modifier;
        }
        if (68 in keysDown && player2.x < canvas.width / 3 - 16 && level[(player2.posy)][player2.posx] === 0) { // Player holding right
            player2.x += player2.speed * modifier;
        }

        // Are they touching?
        if (
                player1.x <= (portalP1.x + 16)
                && portalP1.x <= (player1.x + 16)
                && player1.y <= (portalP1.y + 16)
                && portalP1.y <= (player1.y + 16)
        ) {
            scorePlayer1++;
            reset();
        }
        if (
                player2.x <= (portalP2.x + 16)
                && portalP2.x <= (player2.x + 16)
                && player2.y <= (portalP2.y + 16)
                && portalP2.y <= (player2.y + 16)
        ) {
            scorePlayer2++;
            reset();
        }
    };
    // Draw everything
    var render = function () {
        var i, j, c, d;
        if (tilesetReady && startScreen) {
            for (i = 0, c = 15; i < c; i++) {
                for (j = 0, d = 25; j < d; j++) {
                    ctx.drawImage(tilesetImage, 16, 0, 16, 16, j * 16, i * 16, 16, 16);
                }
            }
            ctx.fillStyle = 'black';
            ctx.font = "32px Comic Sans MS";
            ctx.textAlign = "center";
            ctx.textBaseline = "top";
            ctx.fillText("Squares Among Rocks", 200, 70);
            ctx.font = "10px Comic Sans MS";
            ctx.fillText("Player 1 : arrow keys to move    Player 2 : WASD to move", 200, 180);
        } else if (scorePlayer2 > 7) {
            ctx.fillStyle = 'seagreen';
            ctx.fillRect(0, 0, 400, 240);
            ctx.fillStyle = "rgb(255, 255, 255)";
            ctx.font = "32px Comic Sans MS";
            ctx.textAlign = "center";
            ctx.textBaseline = "top";
            ctx.fillText("Player 2 wins!", 200, 120);
            ctx.font = "12px Comic Sans MS";
            ctx.fillText("Sorry for wasting your time with this crappy failed game", 200, 180);
        } else if (scorePlayer1 > 7) {
            ctx.fillStyle = 'purple';
            ctx.fillRect(0, 0, 400, 240);
            ctx.fillStyle = "rgb(255, 255, 255)";
            ctx.font = "32px Comic Sans MS";
            ctx.textAlign = "center";
            ctx.textBaseline = "top";
            ctx.fillText("Player 1 wins!", 200, 120);
            ctx.font = "14px Comic Sans MS";
            ctx.fillText("Now wasn't that totally epic", 200, 180);
        } else {
            if (tilesetReady) {
                for (i = 0, c = 15; i < c; i++) {
                    for (j = 0, d = 25; j < d; j++) {
                        if (level[i][j] === 1) {
                            ctx.drawImage(tilesetImage, 0, 0, 16, 16, j * 16, (i * 16), 16, 16);
                        } else {
                            ctx.drawImage(tilesetImage, 16, 0, 16, 16, j * 16, (i * 16), 16, 16);
                        }
                    }
                }
            }
            if (portalP1Ready) {
                ctx.drawImage(portalP1Image, portalP1.x, portalP1.y);
            }
            if (portalP2Ready) {
                ctx.drawImage(portalP2Image, portalP2.x, portalP2.y);
            }
            if (player1Ready) {
                ctx.drawImage(player1Image, player1.x, player1.y);
            }
            if (player2Ready) {
                ctx.drawImage(player2Image, player2.x, player2.y);
            }
            // Score
            ctx.fillStyle = "rgb(0, 0, 0)";
            ctx.font = "10px Comic Sans MS";
            ctx.textAlign = "left";
            ctx.textBaseline = "top";
            ctx.fillText("Player 1 : " + scorePlayer1 + "/8", 16, 16);
            ctx.fillText("Player 2 : " + scorePlayer2 + "/8", 320, 16)
        }
        if (scorePlayer1 === -1) {
            scorePlayer1++;
        }
    };
    setInterval(function(){
        player1.posx = Math.ceil(player1.x / 16);
        player1.posy = Math.ceil(player1.y / 16);
        player2.posx = Math.ceil(player2.x / 16);
        player2.posy = Math.ceil(player2.y / 16);
    }, 16);
    // The main game loop
    var main = function () {
        var now = Date.now();
        var delta = now - then;
        update(delta / 1000);
        render();
        then = now;
        // Request to do this again ASAP
        requestAnimationFrame(main);
    };
    // Cross-browser support for requestAnimationFrame
    var w = window;
    requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;
    // Let's play this game!
    var then = Date.now();
    createNewLevel();
    reset();
    main();
};